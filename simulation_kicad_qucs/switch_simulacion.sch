<QucsStudio Schematic 4.3.1>
<Properties>
View=-102,-49,1156,730,1,67,69
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
Pac P2 1 440 200 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 440 230 0 0 0 0
.SP SP1 1 80 370 0 63 0 0 "lin" 1 "1 GHz" 1 "3 GHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "none" 0
GND * 1 380 200 0 0 0 0
SPfile X1 1 380 170 -26 -59 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
L L1 1 280 170 -26 -54 0 0 "1.84 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C1 1 210 200 17 -26 0 1 "1.231 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
Pac P1 1 100 200 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 100 230 0 0 0 0
GND * 1 210 230 0 0 0 0
</Components>
<Wires>
410 170 440 170 "" 0 0 0 ""
100 170 210 170 "" 0 0 0 ""
210 170 250 170 "" 0 0 0 ""
310 170 350 170 "" 0 0 0 ""
</Wires>
<Diagrams>
<Smith 240 480 200 200 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
<Rect 590 370 240 160 31 #c0c0c0 1 00 1 0 0.5 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[2,1])" "" #0000ff 0 3 0 0 0 0 "">
	  <Mkr 2.88889e+09 290 -90 3 1 0 0 0 50>
	  <Mkr 1.11111e+09 70 -200 3 1 0 0 0 50>
</Rect>
</Diagrams>
<Paintings>
</Paintings>
