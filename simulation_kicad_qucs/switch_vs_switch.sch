<QucsStudio Schematic 4.3.1>
<Properties>
View=0,-120,1163,800,1.18182,0,76
Grid=10,10,1
DataSet=*.dat
DataDisplay=
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
GND * 1 490 230 0 0 0 0
SPfile X1 1 490 200 -26 -59 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
Pac P1 1 210 230 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 210 260 0 0 0 0
GND * 1 320 260 0 0 0 0
.SP SP1 1 200 310 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "none" 0
Pac P2 1 920 240 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 920 270 0 0 0 0
SPfile X2 1 650 200 -39 -98 0 2 "PE4259_de_embed_S2P/PE4259_RFC_RF2_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 650 170 0 0 0 2
C C1 1 320 230 17 -26 0 1 "0.920351 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
L L1 1 390 200 -26 -54 0 0 "2.05574nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C2 1 830 250 17 -26 0 1 "1.144 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
GND * 1 830 280 0 0 0 0
L L2 1 750 200 -26 10 0 0 "1.985 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
</Components>
<Wires>
210 200 320 200 "" 0 0 0 ""
420 200 460 200 "" 0 0 0 ""
520 200 620 200 "" 0 0 0 ""
920 200 920 210 "" 0 0 0 ""
680 200 720 200 "" 0 0 0 ""
320 200 360 200 "" 0 0 0 ""
830 200 920 200 "" 0 0 0 ""
830 200 830 220 "" 0 0 0 ""
780 200 830 200 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect 673 540 360 220 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -30 10 1.89282 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[2,1])" "" #ff0000 0 3 0 0 0 0 "">
	<"dB(S[2,2])" "" #ff00ff 0 3 0 0 0 0 "">
</Rect>
<Smith 373 550 220 220 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[2,2]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
</Diagrams>
<Paintings>
</Paintings>
