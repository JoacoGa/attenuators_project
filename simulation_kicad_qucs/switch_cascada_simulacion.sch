<QucsStudio Schematic 4.3.1>
<Properties>
View=0,-197,1776,800,0.605827,3,0
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
Pac P1 1 90 140 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 90 170 0 0 0 0
GND * 1 200 170 0 0 0 0
L L1 1 270 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C1 1 200 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
.SP SP1 1 80 240 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "none" 0
SPfile X1 1 330 110 -33 -79 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 330 140 0 0 0 0
GND * 1 360 170 0 0 0 0
L L2 1 430 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C2 1 360 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X2 1 490 110 -73 -116 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 490 140 0 0 0 0
GND * 1 520 170 0 0 0 0
L L3 1 590 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C3 1 520 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X3 1 650 110 -94 -152 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 650 140 0 0 0 0
GND * 1 680 170 0 0 0 0
L L4 1 750 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C4 1 680 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X4 1 810 110 -160 -187 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 810 140 0 0 0 0
GND * 1 840 170 0 0 0 0
L L5 1 910 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C5 1 840 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X5 1 970 110 -245 -226 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 970 140 0 0 0 0
GND * 1 1000 170 0 0 0 0
L L6 1 1070 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C6 1 1000 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X6 1 1130 110 -338 -259 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 1130 140 0 0 0 0
GND * 1 1160 170 0 0 0 0
L L7 1 1230 110 -26 -54 0 0 "1.869 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
C C7 1 1160 140 17 -26 0 1 "0.912 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
SPfile X7 1 1290 110 -422 -287 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 1290 140 0 0 0 0
GND * 1 1340 170 0 0 0 0
Pac P2 1 1340 140 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
</Components>
<Wires>
90 110 200 110 "" 0 0 0 ""
200 110 240 110 "" 0 0 0 ""
360 110 400 110 "" 0 0 0 ""
520 110 560 110 "" 0 0 0 ""
680 110 720 110 "" 0 0 0 ""
840 110 880 110 "" 0 0 0 ""
1000 110 1040 110 "" 0 0 0 ""
1160 110 1200 110 "" 0 0 0 ""
1320 110 1340 110 "" 0 0 0 ""
</Wires>
<Diagrams>
<Smith 300 440 200 200 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
<Rect 559 432 501 193 31 #c0c0c0 1 00 1 0 0.5 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #0000ff 0 3 0 0 0 0 "">
	  <Mkr 8.40556e+08 201 -152 3 1 0 0 0 50>
	  <Mkr 2.83389e+09 531 -132 3 1 0 0 0 50>
	<"dB(S[2,1])" "" #ff00ff 0 3 0 0 0 0 "">
	  <Mkr 5.08333e+08 141 -202 3 1 0 0 0 50>
	  <Mkr 2.66778e+09 511 -202 3 1 0 0 0 50>
</Rect>
</Diagrams>
<Paintings>
</Paintings>
