<QucsStudio Schematic 4.3.1>
<Properties>
View=0,-10,1340,780,1,0,0
Grid=10,10,1
DataSet=*.dat
DataDisplay=
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
Pac P2 1 440 260 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 440 290 0 0 0 0
GND * 1 380 260 0 0 0 0
SPfile X1 1 380 230 -26 -59 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
Pac P1 1 100 260 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 100 290 0 0 0 0
GND * 1 210 290 0 0 0 0
.SP SP1 1 90 340 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "none" 0
C C1 1 210 260 17 -26 0 1 "0.90 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
L L1 1 280 230 -26 -54 0 0 "2 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
</Components>
<Wires>
410 230 440 230 "" 0 0 0 ""
100 230 210 230 "" 0 0 0 ""
310 230 350 230 "" 0 0 0 ""
210 230 250 230 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect 563 570 360 220 31 #c0c0c0 1 00 1 0 1 1 1 0 1 1 1 0 1 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[2,1])" "" #ff0000 0 3 0 0 0 0 "">
</Rect>
<Smith 313 580 220 220 31 #c0c0c0 1 00 1 0 1 1 1 0 1 1 1 0 1 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
<Smith 923 290 220 220 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[2,2]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
<Rect 973 570 360 220 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -33.0896 10 2.70385 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[2,2])" "" #0000ff 0 3 0 0 0 0 "">
</Rect>
</Diagrams>
<Paintings>
</Paintings>
