<QucsStudio Schematic 4.3.1>
<Properties>
View=0,-51,1063,616,1,0,0
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
Pac P1 1 130 100 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 130 130 0 0 0 0
.SP SP1 1 60 270 0 63 0 0 "lin" 1 "1 GHz" 1 "3 GHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "none" 0
GND * 1 260 100 0 0 0 0
SPfile X1 1 260 70 -43 -93 0 0 "PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 0 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 330 130 0 0 0 0
GND * 1 480 130 0 0 0 0
Pac P2 1 590 100 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 590 130 0 0 0 0
R R1 1 330 100 15 -26 0 1 "866 Ω" 1 "26.85" 0 "european" 0 "SMD0603" 0
R R3 1 480 100 15 -26 0 1 "866 Ω" 1 "26.85" 0 "european" 0 "SMD0603" 0
R R2 1 400 70 -26 -53 0 2 "5.8 Ω" 1 "26.85" 0 "european" 0 "SMD0603" 0
</Components>
<Wires>
130 70 230 70 "" 0 0 0 ""
290 70 330 70 "" 0 0 0 ""
430 70 480 70 "" 0 0 0 ""
330 70 370 70 "" 0 0 0 ""
480 70 590 70 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect 530 400 240 160 31 #c0c0c0 1 00 1 0 0 0 1 0 0 0 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #ff0000 0 3 0 0 0 0 "">
	<"dB(S[1,2])" "" #ff00ff 0 3 0 0 0 0 "">
</Rect>
<Smith 240 420 200 200 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"'atenuador_resistivo':S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
	<"S[1,1]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
</Diagrams>
<Paintings>
</Paintings>
