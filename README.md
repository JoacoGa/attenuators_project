# Attenuators Project

## Project Description:
Design a variable attenuator with a range from 0 dB to 30 dB, adjustable in 1 dB steps.

## Specifications

- Max Frequency: 3 GHz.
- Max Input Power: 0 dBm.
- Impedance (Zin = Zout): 50 Ohms.

## Repository Contents:

- Qucs simulation files
- KiCad schematic files
- PCB layout
- Images of schematic, simulation, and PCB layout
- Components with footprints and S parameters for simulation

## Prerequisites:

- Qucs
- KiCad
- Python3

## Calculations:

![Alt text](pictures/image.png)

![Alt text](pictures/image-1.png)

Add table with value of resistances

## Simulations

To simulate this schematic we consider differents step to simulate:

1. Simulate one switch.
2. Simulate two switches with one attenuator in the middle.
3. Simulate the entire circuit with 5 attenuators.

To make this simulation work, we need to use the S parameters of the switch downloaded from DigiKey. Initially, we ensure the impedance adaptability for each step, aiming for 50 Ohms for both Input and Output Resistances. After the simulation, we observe the need to add an LC filter to compensate for the block (switch, attenuator, switch) at each step.

![Alt text](pictures/image-2.png)
Here, the switch is not adapted. After adding the LC filter (determining specific values iteratively with Qucs), the switch is compensated.
![Alt text](pictures/image-3.png)

In the next picture, we see the second simulation with the compensated circuit.

![Alt text](pictures/image-4.png)

Finally, we perform the last simulation with the entire circuit to ensure impedance compatibility.

![Alt text](pictures/image-5.png)

After this simulation, we proceed to create the schematic in KiCad and then design the PCB. This is the final schematic:

![Alt text](pictures/image-6.png)

![Alt text](pictures/image-7.png)

When working on the PCB, we set up the wire width using the calculator tool in KiCad:

![Alt text](pictures/image-8.png)

And this is the final PCB with a 3D view:

![Alt text](pictures/image-9.png)

![Alt text](pictures/image-10.png)


Note: Consider using a wider width, around 1mm, for Vcc.
